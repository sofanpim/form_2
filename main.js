'use strict'

let form_popup = document.getElementsByClassName("form-request-popup");
let form_request = document.querySelector(".form-request-button-box");
let close_button = document.querySelector(".form-request-popup-close");
let content = document.querySelector(".form-request-popup-content");

form_request.onclick = function () {
    if (form_popup[0].className == "form-request-popup hidden") {
        form_popup[0].className = "form-request-popup see";
    } else if (form_popup[0].className == "form-request-popup see") {
        form_popup[0].className = "form-request-popup hidden";
    }
}

close_button.onclick = () =>{
    form_popup[0].className = "form-request-popup hidden";
}



$("form").submit(function(e){
    e.preventDefault();

    let fioVal = $(this).find("[name='fio']").val();
    let emailVal = $(this).find("[name='email']").val();
    let phoneVal = $(this).find("[name='phone']").val();

    let errorElement = $(this).find(".error-message");

    if(fioVal == "" || emailVal == "" || phoneVal ==""){
// .html -позволяет вставлять текст внутрь html элемента
        var errorMessage = "Вы не заполнили поля";
    
    if(fioVal == ""){
        $(this).find("[name='fio']").css("border-color", "red");
        errorMessage = errorMessage + " ваш фамилия ";
    }else{
        $(this).find("[name='fio']").css("border-color", "green");
    }
    if(emailVal == ""){
        $(this).find("[name='email']").css("border-color", "red");
        errorMessage = errorMessage + " собака@ ";
    }else{
        $(this).find("[name='email']").css("border-color", "green");
    }
    if(phoneVal == ""){
        $(this).find("[name='phone']").css("border-color", "red");
        errorMessage = errorMessage + " телефон ";
    }else{
        $(this).find("[name='phone']").css("border-color", "green");
    }
    errorElement.html(errorMessage);
    errorElement.slideDown();
}else{
    alert("Все классно, форма  отправлена");
    errorElement.slideUp();
    $(this).find("[name='fio']").css("border-color", "green");
    $(this).find("[name='email']").css("border-color", "green");
    $(this).find("[name='phone']").css("border-color", "green");
}

});


$("[name='fio'], [name='email'],[name='phone']").keyup(function(e){
    if(e.keyCode != 27 && e.keyCode !=9 && e.keyCode !=16 && e.keyCode !=17){
        if($(this).val().length >=2 && $(this).val().length <= 30){
            $(this).css("border-color", "green");
        }else{
            $(this).css("border-color", "red");
        }
    }
});

//функция явления формы народу
$(window).keydown(function (e) { 
    if(e.keyCode == 220){
        form_popup[0].className = "form-request-popup shake animated";
        setTimeout(() => {
            form_popup[0].className = "form-request-popup see";
        }, 1000);
    }

});

$(window).keydown(function (e) { 
    if(e.keyCode == 27){
        form_popup[0].className = "form-request-popup zoomOut animated slow";
        setTimeout(() => {
            form_popup[0].className = "form-request-popup hidden";
        }, 2010);
    }

});


$(window).keydown(function(e){
  if(e.keyCode == 32 || e.keyCode == 190 ){
    $("[type='submit']").click();
  }    
})
$(window).keydown(function(e){
  if(e.keyCode == 49){
    content.className = "alt_dark";
  }
})
$(window).keydown(function(e){
  if(e.keyCode == 50){
    content.className = "alt_red";
  }
})
//js
let faq_answer=document.querySelector(".faq_answer");
let faq_quest=document.querySelector(".faq_quest");
/*faq_quest.onclick =()=>{
if(faq_answer.classList.contains("none"));{
    faq_answer.classList.remove("none");
}
if   (faq_answer.classList.contains("slideInDown")){
    faq_answer.classList.add("slideOutUp");
    faq_answer.classList.remove("slideInDown");
}
else if(faq_answer.classList.contains("slideOutUp")){
    faq_answer.classList.add("slideInDown");
    faq_answer.classList.remove("slideOutUp");
}
    {
    faq_answer.classList.remove("slideOutUp");
}
}*/
$(".faq_quest").click(function(){
    $(this).next().toggle(function(){
       // Animation
    })

})